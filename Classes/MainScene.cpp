#include "MainScene.h"
#include "ResourcePreloader.h"

USING_NS_CC;

bool MainScene::init(){
    /* -------------- percentage percentageLabel --------------*/
    Label* percentageLabel = Label::createWithTTF("", "fonts/arial.ttf", 50);
    percentageLabel->setPosition(Vec2(1136 / 2, 640 / 2));
    this->addChild(percentageLabel);


    /* -------------- complete loading percentageLabel --------------*/
    Label* completeLoadingLabel = Label::createWithTTF("Complete Loading", "fonts/arial.ttf", 50);
    completeLoadingLabel->setPosition(Vec2(1136 / 2, 640 / 2 - 100));
    completeLoadingLabel->setVisible(false);
    this->addChild(completeLoadingLabel);


    /* -------------- create resource preloader --------------*/
    ResourcePreloader* resourcePreloader = ResourcePreloader::create();
    resourcePreloader->setEventListener(
        [percentageLabel, completeLoadingLabel](ResourcePreloader::EVENT event, void* customData = nullptr){
            switch (event){
                case ResourcePreloader::COMPLETE_LOADING_ONE:{
                    float percentage = *static_cast<float*>(customData);
                    percentageLabel->setString(
                        "Loading Percentage: " + std::to_string(static_cast<int>(percentage)) + "%"
                    );
                    break;
                };

                case ResourcePreloader::COMPLETE_LOADING_ALL:{
                    completeLoadingLabel->setVisible(true);
                    break;
                };

                default:
                    break;
            }
        }
    );
    this->addChild(resourcePreloader);


    /* -------------- add resources --------------*/
    for (int i = 0; i <= 16; i++){
        resourcePreloader->addImage("images/image_" + std::to_string(i) + ".png");
    };
    for (int i = 0; i <= 7; i++){
        resourcePreloader->addPlist("plists/plist_" + std::to_string(i) + ".plist");
    };
    for (int i = 0; i <= 19; i++){
        resourcePreloader->addAudio("audios/audio_" + std::to_string(i) + ".mp3");
    };


    /* -------------- delay loading --------------*/
    this->scheduleOnce([resourcePreloader](float _){
        resourcePreloader->startLoading();
    }, 2, "StartLoadDelay");

    return true;
};