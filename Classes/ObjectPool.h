#ifndef __OBJECT_POOL_H__
#define __OBJECT_POOL_H__

#include "cocos2d.h"

template <class TmpObject>
class ObjectPool : public cocos2d::Node{   
    public:
        // typedef
        typedef std::function<TmpObject()> CreatePoolObjectFunc;

        // creator
        static ObjectPool* create(CreatePoolObjectFunc createObjectFuc, int initialAmount, bool isCocosObject){
            ObjectPool* instance = new(std::nothrow) ObjectPool(createObjectFuc, initialAmount, isCocosObject);

            if (instance){ 
                return instance;
            }else{
                delete instance;
                instance = nullptr;
                return nullptr;
            };
        };  

        ObjectPool(CreatePoolObjectFunc createObjectFuc, int initialAmount, bool isCocosObject){
            _createObjectFuc = createObjectFuc;
            _initialAmount = initialAmount;
            _isCocosObject = isCocosObject;

            _objectsQueue = this->_initObjectsQueue(createObjectFuc, initialAmount, isCocosObject);
        };

        TmpObject getObject(){
            if (_objectsQueue.empty()){
                CCLOG("Create New Object");
                return _createObjectFuc();
            };

            TmpObject obj = _objectsQueue.front();
            _objectsQueue.pop();
            if (_isCocosObject){
                obj->removeFromParent();
            };
            return obj;
        };
    
        void releaseObject(TmpObject obj){
            if (_objectsQueue.size() == _initialAmount){
                CCLOG("Pool is Full");
                return;
            };

            if (_isCocosObject){
                this->addChild(obj);
            };
            _objectsQueue.push(obj);
        };

        void cleanUp(){
            if (_isCocosObject){
                this->removeAllChildren();
            };
            
            std::queue<TmpObject> emptyQueue;
            std::swap(_objectsQueue, emptyQueue);
        };

    private:
        // variable
        CreatePoolObjectFunc _createObjectFuc;
        int _initialAmount;
        bool _isCocosObject;
        std::queue<TmpObject> _objectsQueue;

        // function
        std::queue<TmpObject> _initObjectsQueue(CreatePoolObjectFunc createObjectFuc, int initialAmount, bool isCocosObject){
            std::queue<TmpObject> objectsQueue = {};
            for (int i = 0; i < initialAmount; i++){
                TmpObject obj = createObjectFuc();
                if (isCocosObject){
                    this->addChild(obj);
                };
                objectsQueue.push(obj);
            };
            return objectsQueue;
        };
};

#endif // __OBJECT_POOL_H__
