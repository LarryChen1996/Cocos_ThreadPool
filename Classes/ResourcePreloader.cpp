#include "ResourcePreloader.h"
#include "editor-support/cocostudio/SimpleAudioEngine.h"
#include "ThreadPool.h"

USING_NS_CC;

/* ---------------------------------------------  Creator  --------------------------------------------- */

bool ResourcePreloader::init(){
    _threadPool = new (std::nothrow) ThreadPool();
    _threadPool->start();

    return true;
};

/* ---------------------------------------------  Public  --------------------------------------------- */

void ResourcePreloader::addImage(std::string filePath){
    if (this->_isExistInVector(_imageFilePaths, filePath)){
        return;
    };

    _imageFilePaths.push_back(filePath);
};

void ResourcePreloader::addPlist(std::string filePath){
    if (this->_isExistInVector(_plistFilePaths, filePath)){
        return;
    };

    _plistFilePaths.push_back(filePath);
};

void ResourcePreloader::addAudio(std::string filePath){
    if (this->_isExistInVector(_audioFilePaths, filePath)){
        return;
    };

    _audioFilePaths.push_back(filePath);
};

void ResourcePreloader::startLoading(){
    _resCount = _imageFilePaths.size() + _plistFilePaths.size() + _audioFilePaths.size();
    if (_resCount == 0){
        this->_dispatchEvent(ResourcePreloader::COMPLETE_LOADING_ALL);
        return;
    };

    _threadPool->addTask(std::bind(&ResourcePreloader::_startPreloadImages, this));
    _threadPool->addTask(std::bind(&ResourcePreloader::_startPreloadPlists, this));
    _threadPool->addTask(std::bind(&ResourcePreloader::_startPreloadAudios, this));
};

void ResourcePreloader::setEventListener(ResourcePreloaderEventListener eventListener){
    _eventListener = eventListener;
};

/* ---------------------------------------------  Private  --------------------------------------------- */

void ResourcePreloader::_startPreloadImages(){
    auto textureCache = Director::getInstance()->getTextureCache();

    for (std::vector<std::string>::iterator it = _imageFilePaths.begin(); it != _imageFilePaths.end(); it++){
        textureCache->addImage(*it);
        this->_completeLoading(*it);
    };
};

void ResourcePreloader::_startPreloadPlists(){
    auto spriteFrameCache = SpriteFrameCache::getInstance();

    for (std::vector<std::string>::iterator it = _plistFilePaths.begin(); it != _plistFilePaths.end(); it++){
        spriteFrameCache->addSpriteFramesWithFile(*it);
        this->_completeLoading(*it);
    };
};

void ResourcePreloader::_startPreloadAudios(){
    auto audioEngine = CocosDenshion::SimpleAudioEngine::getInstance();

    for (std::vector<std::string>::iterator it = _audioFilePaths.begin(); it != _audioFilePaths.end(); it++){
        audioEngine->preloadEffect((*it).c_str());
        this->_completeLoading(*it);
    };
};

void ResourcePreloader::_completeLoading(std::string filePath){
    std::lock_guard<std::mutex> guard(_completeLoadingFuncMutex);

    CCLOG("Complete Loading: %s", filePath.c_str());
    _completePreloadResCount = _completePreloadResCount + 1;
    float percentage = static_cast<float>(_completePreloadResCount) / static_cast<float>(_resCount);
    percentage = percentage * 100;
    this->_dispatchEvent(COMPLETE_LOADING_ONE, static_cast<float*>(&percentage));
    if (_completePreloadResCount == _resCount){
        this->_dispatchEvent(COMPLETE_LOADING_ALL);
    };
};

void ResourcePreloader::_dispatchEvent(ResourcePreloader::EVENT event, void* customData){
    if (_eventListener != nullptr){
        _eventListener(event, customData);
    };
};