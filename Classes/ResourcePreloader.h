#ifndef __RESOURCE_PRELOADER_H__
#define __RESOURCE_PRELOADER_H__

#include "cocos2d.h"
#include "ThreadPool.h"

class ResourcePreloader : public cocos2d::Node{    
    public:
        // creator
        CREATE_FUNC(ResourcePreloader);
        virtual bool init();

        // class const variable
        enum EVENT {
            COMPLETE_LOADING_ONE,
            COMPLETE_LOADING_ALL,
        };

        // typedef
        typedef std::function<void(ResourcePreloader::EVENT, void*)> ResourcePreloaderEventListener;

        void addImage(std::string filePath);
        void addPlist(std::string filePath);
        void addAudio(std::string filePath);
        void startLoading();
        void setEventListener(ResourcePreloaderEventListener eventListener);
    
    private:
        // variable
        int _resCount;
        int _completePreloadResCount;
        ResourcePreloaderEventListener _eventListener;
        std::vector<std::string> _imageFilePaths;
        std::vector<std::string> _plistFilePaths;
        std::vector<std::string> _audioFilePaths;
        std::mutex _completeLoadingFuncMutex;
        ThreadPool* _threadPool;

        // function
        template <typename T>
        bool _isExistInVector(std::vector<T> vector, T value);

        void _startPreloadImages();
        void _startPreloadPlists();
        void _startPreloadAudios();
        void _completeLoading(std::string filePath);
        void _dispatchEvent(ResourcePreloader::EVENT event, void* customData = nullptr);
};

template <typename T>
bool ResourcePreloader::_isExistInVector(std::vector<T> vector, T value){
    return std::find(vector.begin(), vector.end(), value) != vector.end();
};

#endif // __RESOURCE_PRELOADER_H__
