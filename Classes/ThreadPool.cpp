#include "ThreadPool.h"

const int ThreadPool::MAX_THREAD_AMOUNT = 3;

/* ---------------------------------------------  Creator  --------------------------------------------- */

ThreadPool* ThreadPool::create(){
    ThreadPool* instance = new (std::nothrow) ThreadPool();

    if (instance){ 
        return instance;
    }else{
        delete instance;
        instance = nullptr;
        return nullptr;
    };
};

ThreadPool::ThreadPool(){
    _isStart = false;           // 控制變數，用於控制 thread 是否執行
    _currentTaskRemaining = 0;  // 記錄變數，紀錄當前還在執行的任務數量
};

ThreadPool::~ThreadPool(){
    this->stop();
};

/* ---------------------------------------------  public  --------------------------------------------- */


void ThreadPool::start(){
    if (_isStart){
        return;
    };

    _isStart = true;
    for (int i = 0; i < ThreadPool::MAX_THREAD_AMOUNT; i++){
        _threadQueue.push(std::thread(std::bind(&ThreadPool::_runTask, this)));
    };
};

/*
    注意 !
    不可在 task 內呼叫 stop，避免執行該 task 的 thread 產生互相等待情形造成死結。
*/
void ThreadPool::stop(){
    std::unique_lock<std::mutex> lock(_mx);

    // 清除佇列內所有 task
    std::queue<TaskFunc> emptyTaskQueue;
    std::swap(_taskQueue, emptyTaskQueue);

    // 改變環境變數，使 thread 跳出無窮迴圈函式
    _isStart = false;

    // 通知所有 thread 開始執行 task
    _cv.notify_all();
    
    // 等待所有 thread 執行完當前任務
    _cv_finished.wait(lock, [this](){ 
        return _currentTaskRemaining == 0; 
    });

    // join 所有的 thread 並清空佇列內所有 thread
    while(!_threadQueue.empty()){
        _threadQueue.front().join();
        _threadQueue.pop();
    };
};

void ThreadPool::addTask(TaskFunc task){
    // _taskQueue 為共享變數，更改前需進行上鎖
    std::unique_lock<std::mutex> lock(_mx);
    _taskQueue.push(task);
    lock.unlock();

    if (_isStart){
        // 通知隨機一條 thread 開始執行 task
        _cv.notify_one();
    };
};  

/* ---------------------------------------------  private  --------------------------------------------- */

void ThreadPool::_runTask(){
    while (true){
        std::unique_lock<std::mutex> lock(_mx);

        /* 
            尋找是否有 task 可做，若無則進入等待狀態並釋放鎖 
            ( condition_variable.wait(lock) 進入 wait 狀態時，該鎖 (lock) 會被釋放) 
        */
        if (_taskQueue.empty()){
            _cv.wait(lock);
        };

        // 控制變數，若 _isStart 為 false 時則結束此無窮迴圈函式 (執行緒結束)
        if (!_isStart){
            return;
        };
    
        // 取出第一個任務
        TaskFunc task = _taskQueue.front();
        _taskQueue.pop();
        _currentTaskRemaining = _currentTaskRemaining + 1;

        /*
            為何這裡要釋放鎖呢?
            因為在任務執行過程中，不涉及任何 race condition，並且我們並不知道任務執行的時長 (可能會很長)，
            所以我們應該先釋放鎖，讓其他執行緒可以訪問共享變數。
        */
        lock.unlock();
        task();

        // 要改變共享變數 _currentTaskRemaining，重新鎖上
        lock.lock();
        _currentTaskRemaining = _currentTaskRemaining - 1;
    
        // 通知 _cv_finished 已完成一個 task
        _cv_finished.notify_one();
    };
};

