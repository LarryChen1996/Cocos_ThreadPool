#ifndef __THREAD_POOL_H__
#define __THREAD_POOL_H__

#include "cocos2d.h"

typedef std::function<void()> TaskFunc;

class ThreadPool{   
    public:
        // creator
        static ThreadPool* create();
        ThreadPool();
        ~ThreadPool();

        // function
        void start();
        void stop();
        void addTask(TaskFunc task);

    private:
        // class static const variable
        static const int MAX_THREAD_AMOUNT;

        // variable
        bool _isStart;
        int _currentTaskRemaining;
        std::condition_variable _cv;
        std::condition_variable _cv_finished;
        std::mutex _mx;
        std::queue<TaskFunc> _taskQueue;
        std::queue<std::thread> _threadQueue;

        // function
        void _runTask();
};

#endif // __THREAD_POOL_H__
