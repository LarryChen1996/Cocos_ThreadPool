# thread 基本用法

```c++
    myFuncyion(){
        CCLOG('doSomething')
    };

    main(){
        // 建立帶入函式，執行緒在此啟動
        std::thread t1 = std::thread(myFuncyion);

        // 在此等待 t1 執行緒任務 (myFuncyion) 完畢，才會繼續往下走
        t1.join();

        // 在此不等待 t1 執行緒任務 (myFuncyion)，此時 t1 可視為 daemon process (後台執行程式)
        t1.detach();

        /* 
            注意 !
            join 與 detach 不可同時使用，只可擇一
        */
    }
```

# ThreadPool 實作時所遇到問題

Q: `無法理解為何要讓 thread 執行一個無窮迴圈的函式 ?`

A: 因為執行緒在函式結束時即釋放，因此若不使用無窮迴圈就無法保持該執行緒持續運作。

---

Q: `何時要使用到 mutex (鎖) ?`

A: 當多個執行緒會同時用到共用的資源 (ex: ThreadPool 內的變數 _taskQueue 及 _currentTaskRemaining)，為避免產生 data race 時，限制同時 (Synchronize) 只能有一個執行緒存取。

---

Q: `condition_variable (ThreadPool 內的變數 _cv) 是什麼 ?`

A: 可控制該執行緒函式是否暫停及開始。 
- wait: 阻塞當前執行緒直到條件變量被喚醒
- notify_one: 通知一個正在等待的執行緒
- notify_all: 通知所有正在等待的執行緒

重要事項: `cv.wait(lock) 該 lock 在進入 wait 時即被釋放，等到執行緒被通知啟動後 (notify_one、notifyall) 才會重新鎖上`。

---

Q: `ThreadPool 內的變數 _isStart 是做什麼用 ?`

A: 用來控制該執行緒是否要結束。在上述有提到，執行緒內的函式若結束，也就代表執行緒結束並釋放，因次用 _isStart 此變數來控制執行緒是否要結束。

---

Q: `如何避免 DeadLock (互相等待的情形) 產生 ?`

A: 

- `非必要時盡量不要用多個鎖`，若必須時則注意多個鎖 (mutex) 的`使用順序`，看以下範例

```c++
std::mutex mu1;
std::mutex mu2;

function doSomething1(){
    std::lock_guard lock(mu1);
    std::lock_guard lock(mu2);
    
    // do things
};

function doSomething2(){
    std::lock_guard lock(mu2);
    std::lock_guard lock(mu1);

    // do things
};

function main(){
    std::thread t1 = std::thread(doSomething1);
    std::thread t2 = std::thread(doSomething2);
};
```

此時 doSomething1 鎖著 m1 在等 m2，而 doSomething2 鎖著 m2 在等 m1，造成 doSomething1 在等 doSomething2 的 mu2 釋放，而 doSomething2 在等 doSomething1 的 mu1 釋放。

`此情況可將鎖的順序改為一致即可。` (先鎖 mu1 再 mu2 或 先鎖 mu2 再 mu1)

<br>

- 執行緒內的函式 join 自己，看以下範例

```c++
std::thread t1;

function doSomething(){
    t1.join();

    // do things
};

function main(){
    t1 = std::move(std::thread(doSomething));
};
```

此時 t1 執行的函式 doSomething 在等待 doSomething 函式結束，造成互相等待情況。

`在使用 ThreadPool 時，若在 ThreadPool 內的 task 呼叫 stop，即會發生 DeadLock。`